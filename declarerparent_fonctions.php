<?php
/**
 * Fonctions utiles au lugin Déclarer parent
 *
 * @plugin     Déclarer parent
 * @copyright  2017
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Declarerparent\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cherche le contenu parent d'un contenu précis
 *
 * @param string $objet
 * @param int|string $id_objet
 * @return array
 */
function filtre_objet_trouver_parent_dist($objet, $id_objet) {
	// compatibilite signature inversee
	if (is_numeric($objet) and !is_numeric($id_objet)) {
		list($objet, $id_objet) = [$id_objet, $objet];
	}
	include_spip('base/objets_parents');
	return objet_trouver_parent($objet, $id_objet);
}


/**
 * Cherche les enfants d'un contenu précis
 *
 * @param string $objet
 * @param int|string $id_objet
 * @return array
 */
function filtre_objet_trouver_enfants_dist($objet, $id_objet) {
	// compatibilite signature inversee
	if (is_numeric($objet) and !is_numeric($id_objet)) {
		list($objet, $id_objet) = [$id_objet, $objet];
	}
	include_spip('base/objets_parents');
	return objet_trouver_enfants($objet, $id_objet);
}
