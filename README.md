# Plugin Déclarer parent

Ce plugin est normalement obsolète à partir de SPIP 4.

Il faut utiliser les fonctions :
``` php
objet_lister_enfants()
objet_lister_parents()
```

Attention que la liste des parents n'est plus la même, dans SPIP ça peut retourner plusieurs parents à la fois. Pour retrouver le même résultat qu'avant, il faut donc prendre le premier parent de la liste retournée.


``` php
// SPIP4
if (intval(_SPIP_VERSION_ID) >= 30300){
	include_spip('base/objets');
	$info_parent = objet_lister_parents($objet, $id_objet);
	$info_parent = $info_parent[0];
}
// SPIP3 (utiliser dans ce cas le plugin declarerparent)
elseif (test_plugin_actif('declarerparent')){		
	include_spip( 'base/objets_parents' );
	$info_parent = objet_trouver_parent($objet, $id_objet);
}
```