<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'declarerparent_description' => 'Cette fonctionnalité a vocation à intégrer le noyau de l’API de déclaration des objets. En attendant https://core.spip.net/issues/3844',
	'declarerparent_nom' => 'Déclarer le parent',
	'declarerparent_slogan' => 'Définir la relation à l‘objet parent dans la déclaration des objets',
);
